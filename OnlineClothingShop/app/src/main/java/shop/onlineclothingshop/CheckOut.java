package shop.onlineclothingshop;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CheckOut extends AppCompatActivity {
    Context context;

    EditText firstNameEditText , lastNameEditText , phoneEditText , addressEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Address");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        context = CheckOut.this;
        firstNameEditText = (EditText)findViewById(R.id.first_name);
        lastNameEditText = (EditText)findViewById(R.id.last_name);
        phoneEditText = (EditText)findViewById(R.id.phone);
        addressEditText = (EditText)findViewById(R.id.address);

    }

    public void order(View v)
    {
        if(firstNameEditText.getText().toString().equals("")||lastNameEditText.getText().toString().equals("")||phoneEditText.getText().toString().equals("")||addressEditText.getText().toString().equals(""))
        {
            Toast.makeText(context , "Please fill all fields !" , Toast.LENGTH_LONG).show();
        }
        else {


            sendEmail();
        }
    }

    private void sendEmail() {
        Log.i("Send email", "");

        String[] TO = {"mehdirawjani110@gmail.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");


        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Order");

        String address = "Name:"+firstNameEditText.getText().toString()+" "+lastNameEditText.getText().toString();
        address = address+"\nPhone Number:"+phoneEditText.getText().toString();
        address = address+"\nAddress:"+addressEditText.getText().toString();
        emailIntent.putExtra(Intent.EXTRA_TEXT, address);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Toast.makeText(context , "Email Sent" , Toast.LENGTH_LONG).show();
            ShopModel.orderList = new ArrayList<>();
            Toast.makeText(context , "Order Placed Successfully" , Toast.LENGTH_LONG).show();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context,
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

}
