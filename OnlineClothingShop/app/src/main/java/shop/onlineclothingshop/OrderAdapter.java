package shop.onlineclothingshop;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    Context context;

    private List<ShopModel> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, price, genre;
        public Button bucket;
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            bucket = (Button) view.findViewById(R.id.add_to_bucket);
            imageView = (ImageView)view.findViewById(R.id.imv_items);
        }
    }


    public OrderAdapter(Context context , List<ShopModel> moviesList) {
        this.context = context;
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.imageView.setImageResource(moviesList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
