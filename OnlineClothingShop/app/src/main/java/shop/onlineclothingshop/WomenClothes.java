package shop.onlineclothingshop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class WomenClothes extends AppCompatActivity {
    Context context;
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    ShopModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("Women Clothes");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        context = WomenClothes.this;

        Button checkButton = (Button) toolbar.findViewById(R.id.check_out);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new ShopModel(context).getOrder().size()!=0)
                {
                    startActivity(new Intent(context , Order.class));
                }
                else
                {
                    Toast.makeText(context, "Please Add items to bucket", Toast.LENGTH_LONG).show();
                }

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MoviesAdapter(movieList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (position)
                {

                    case 0:
                        model = new ShopModel(context);
                        model.setWomanShirts();
                        model.getData();
                        startActivity(new Intent(context , ShopItems.class).putExtra("title" , "Woman Shirts"));
                        break;

                    case 1:
                        model = new ShopModel(context);
                        model.setWomenTShirts();
                        model.getData();
                        startActivity(new Intent(context, ShopItems.class).putExtra("title" , "Woman T Shirts"));
                        break;

                    case 2:
                        model = new ShopModel(context);
                        model.setWomanPents();
                        model.getData();
                        startActivity(new Intent(context, ShopItems.class).putExtra("title" , "Woman Pents"));
                        break;

                    case 3:
                        model = new ShopModel(context);
                        model.setWomanDress();
                        model.getData();
                        startActivity(new Intent(context, ShopItems.class).putExtra("title" , "Woman Dress"));
                        break;

                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareMovieData();
    }

    private void prepareMovieData() {
        Movie movie = new Movie("Shirts");
        movieList.add(movie);

        movie = new Movie("T Shirts");
        movieList.add(movie);

        movie = new Movie("Pents");
        movieList.add(movie);

        movie = new Movie("Dress");
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private WomenClothes.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final WomenClothes.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
