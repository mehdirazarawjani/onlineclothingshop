package shop.onlineclothingshop;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class ShopModel
{
    Context context;
    ShopModel shopModel;
    String itemDescription;
    String itemPrice;
    Integer image;
    public static ArrayList<ShopModel> list;
    public static ArrayList<ShopModel> orderList = new ArrayList<>();

    public ShopModel()
    {

    }
    public ShopModel(Context context)
    {
        this.context = context;
    }

    public void setMenShirts() {
        list = new ArrayList<>();
        setData(R.drawable.men_t_shirt_one);
        setData(R.drawable.men_t_shirt_two);
        setData(R.drawable.men_t_shirt_three);
        setData( R.drawable.men_t_shirt_four);
        setData(R.drawable.men_t_shirt_five);
    }

    public void setMenTShirts()
    {
        list = new ArrayList<>();
        setData( R.drawable.men_shirt_one);
        setData( R.drawable.men_shirt_two);
        setData(R.drawable.men_shirt_three);
        setData( R.drawable.men_shirt_four);
        setData( R.drawable.men_shirt_five);
    }
    public void setMenPents()
    {
        list = new ArrayList<>();
        setData( R.drawable.men_pents_one);
        setData( R.drawable.men_pents_two);
        setData( R.drawable.men_pents_three);
        setData( R.drawable.men_pents_four);
        setData( R.drawable.men_pents_five);
    }

    public void setMenShoes()
    {
        list = new ArrayList<>();
        setData( R.drawable.men_shoes_one);
        setData( R.drawable.men_shoes_two);
        setData( R.drawable.men_shoes_three);
        setData( R.drawable.men_shoes_four);
        setData( R.drawable.men_shoes_five);
    }

    public void setWomanShirts()
    {
        list = new ArrayList<>();
        setData( R.drawable.woman_shirt_one);
        setData( R.drawable.woman_shirt_two);
        setData( R.drawable.woman_shirt_three);
        setData( R.drawable.woman_shirt_four);
        setData( R.drawable.woman_shirt_five);
    }

    public void setWomenTShirts()
    {
        list = new ArrayList<>();
        setData( R.drawable.women_t_shirt_one);
        setData( R.drawable.women_t_shirt_two);
        setData( R.drawable.women_t_shirt_three);
        setData( R.drawable.women_t_shirt_four);
        setData( R.drawable.women_t_shirt_five);
    }

    public void setWomanPents()
    {
        list = new ArrayList<>();
        setData( R.drawable.woman_pent_one);
        setData( R.drawable.woman_pent_two);
        setData( R.drawable.woman_pent_three);
        setData(R.drawable.woman_pent_four);
        setData(R.drawable.woman_pent_five);
    }
    public void setWomanDress()
    {
        list = new ArrayList<>();
        setData( R.drawable.woman_dress_one);
        setData( R.drawable.woman_dress_two);
        setData( R.drawable.woman_dress_three);
        setData( R.drawable.woman_dress_four);
        setData( R.drawable.woman_dress_five);
    }
    public void setWomanShoes()
    {
        list = new ArrayList<>();
        setData(R.drawable.woman_shoes_one);
        setData(R.drawable.woman_shoes_two);
        setData( R.drawable.woman_shoes_three);
        setData( R.drawable.woman_shoes_four);
        setData( R.drawable.woman_shoes_five);
    }

    public void setChildrenDress()
    {
        list = new ArrayList<>();
        setData( R.drawable.childre_clothes_one);
        setData( R.drawable.childre_clothes_two);
        setData( R.drawable.childre_clothes_three);
        setData( R.drawable.childre_clothes_four);
        setData( R.drawable.childre_clothes_five);
    }

    public void setChildrenShoes()
    {
        list = new ArrayList<>();
        setData( R.drawable.childre_shoes_one);
        setData( R.drawable.childre_shoes_two);
        setData( R.drawable.childre_shoes_three);
        setData( R.drawable.childre_shoes_four);
        setData( R.drawable.childre_shoes_five);
    }

    private void setData(Integer s1) {
        shopModel = new ShopModel();
        shopModel.setImage(s1);
        list.add(shopModel);
    }

    public ArrayList<ShopModel> getData()
    {
        return list;
    }

    public void setOrder(Integer e)
    {
        shopModel = new ShopModel();
        shopModel.setImage(e);
        orderList.add(shopModel);

    }
    public ArrayList<ShopModel> getOrder()
    {
        return orderList;
    }


    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
